function [HXY, HX, HY, HXcondY, HYcondX] = entropiaconjunta(P)
%Calculo de la entropia conjunta de la variable XY, expresada por una 
% matriz P con la funcion de masa conjunta, siendo P(i,j)=prob(X=i,Y=j)   
[n,m]= size(P);
HXY=zeros(n,m);
HX=zeros(1,n);
HY=zeros(1,m);
HXcondY=zeros(n,m);
HYcondX=zeros(n,m);
pYcondX=zeros(n,m);
pXcondY=zeros(n,m);
srows=sum(P,1);
scolumns=sum(P,2);
for i=1:n
    suma=0
    for j=1:m
        suma=-P(i,j)*log2(P(i,j));
        HXY(i,j)=suma;
        HY(j)=HY(j)+suma;
        HX(i)=HX(i)+suma;
        pYcondX(i,j)=(P(i,j)/srows(j))
        pXcondY(i,j)=(P(i,j)/scolumns(i))
        HXcondY(i)=HXcondY(i)-pXcondY(i,j)*log2(pXcondY(i,j))
        HYcondX(j)=HYcondX(j)-pYcondX(i,j)*log2(pYcondX(i,j))
    end
end
%HXcondY=-pXcondY*log2(pXcondY)
%HYcondX=-pYcondX*log2(pYcondX)
end




%H(X|Y)=sum(P(X,Y)*log2(P(X,Y)/(P(X)*P(Y)))