function H=entropia(freq)
% Calculates the entropy of a random variable X whose mass 
% is given by the freq vector 
    

    bi = sum(freq.*log2(freq));

    if isnan(bi) 
        bi=0;
        for c = 1:length(freq)
            if freq(c)== 0
                bi= bi+0;
            else
                a=freq(c);
                bi = bi+a*log2(a);
            end
        end 
    end
    H = -bi;
end