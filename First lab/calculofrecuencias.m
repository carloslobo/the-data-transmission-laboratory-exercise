function freq = calculofrecuencias ( nombre_fichero )
% Calculation of the occurrence frequency of each character
% In the indicated name. Returns array of 256 positions
% Indexed by the corresponding character

% Code
%open file read file byte by byte
%in matlab first position is 1
%create a field of 256 positions set all to 0 then ++ to every position+1
%that we have read (fopen(�nombre de file`, `r`) -> fread(fid, 1) function)
% around 4.5
file = fopen(nombre_fichero,'r')
f=zeros(1,256);
while 1
    if feof(file)
        break
    end
    c = fread(file,1);
    numbers = double(c);
  %  sumofletters = sum(numbers);
  %  f(sumofletters) = f(sumofletters) + 1;
   f(c) = f(c) + 1;
    f(c);
end

div = sum(f)
freq = zeros(1,256);
freq = f/div; 
% Include graphical representation of frequencies

figure;
plot(freq);
Entrop = entropia(freq)

end
