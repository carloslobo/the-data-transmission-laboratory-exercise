function brv=binomal_random_variable(number,p)
n = number;
i=1
for pik=p 
    for k = 1:(n+1) 
            field(k) = nchoosek(n,k-1)*pik^(k-1)*(1-pik)^(n-k+1);

    end
    results(i)=entropia(field);
    i=i+1;
end
figure;
results
plot(p,results)
end