function [valor, bloquep] = paridad(bloque)
lenbl = length(bloque);
valor = 0;
for i = 1:lenbl
    valor = str2num(bloque(i)) + valor ;
end
valor = char(string(rem(valor,2)));
bloquep = char([bloque, valor]);
bloque;
end