function [r, tasa] = example331
N = 1000;
f = ascii('hola mundo');
exito = 0;
temp='';
%for R = 3:2:11
for R = 2:2:10
    exito = 0;
    for i=1:N
        %tx = f;% TO FILL IN. TX IS THE FLOW TO TRANSMIT
        tx = repeticion(f, R);
        [rx, errorrate] = canalBS(tx, 0.9); % THE FLOW IN RECEPTION IS OBTAINED
        len = length(rx);
        rx=string(rx);
        temp=char(correction(rx, R));
        rxcorregido = repeticion(temp,R);
        if strcmp(rxcorregido,tx) %CHECKING
            exito = exito + 1;
        end
    end
    errorrate; 
    exito;
    R
    r = exito / N
    tasa =  r*1000/R
end
end