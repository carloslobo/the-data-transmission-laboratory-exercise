function [code, fblock] = hamming(eblock)
eblock = char(eblock);
ps=[eblock(1),eblock(2),eblock(4)];
ss=[eblock(1),eblock(3),eblock(4)];
ts=[eblock(2),eblock(3),eblock(4)];
code = [paridad(ps),paridad(ss),paridad(ts)];
fblock = [code(1),code(2),eblock(1),code(3),eblock(2),eblock(3),eblock(4)];
end