function [tasa, r]= example32()
N = 1000;
f = ascii('hola mundo');
exito = 0;
for i=1:N
    tx = f;% TO FILL IN. TX IS THE FLOW TO TRANSMIT
    rx = canalBS(tx, 0.9); % THE FLOW IN RECEPTION IS OBTAINED
    if strcmp(rx,tx) % CHECK IF TX WAS CORRECT
        exito = exito + 1;
    end
end
r = exito / N
tasa = r*1000
end