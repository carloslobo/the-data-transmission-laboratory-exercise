function [temp] = correction(rx, R)
temp='';
rx=char(rx);
len = length(rx);
for k = 1:R:len
   suma = 0;
   for j = 0:R-1
       if rx(k+j) == '1'
       suma = suma + 1;
       end
   end
   if (suma > (floor(R-1)/2))
       temp = string(temp) + 1;
   else
       temp = string(temp) + 0;
   end
   temp;
end