function [r, tasa] = example333()
N = 1000;
f = ascii('hola mundo');
for R = 3:2:11
exito = 0;
transmisiones = 0;
mensajes = 0;
for i=1:N
    tx = repeticion(f, R);
    mensajes = mensajes + 1;
    while true
        transmisiones = transmisiones + 1;
        rx = canalBS(tx, 0.9);
        rxcorrected = char(repeticion(char(correction(rx, R)), R));
        errornocorregible = strcmp(rxcorrected,tx);
        if errornocorregible
            break;
        end
    end
    if strcmp(rxcorrected,tx)
        exito = exito + 1;
    end
end
R
r = exito / N
t = transmisiones / mensajes
tasa = (r*1000)/(R*t) % CALCULATE THE RATE
end
end