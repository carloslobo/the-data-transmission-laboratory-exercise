function [r,tasa] = example34
N = 1000;
f = ascii('hola mundo'); 
for P = 1:5
    B = pow2(P)
    exito = 0;
    transmissions = 0;
    tx=f(1:B);
    mensajes = 0;
    for i=1:N
        mensajes = mensajes + 1;
        [tvalor, txt] = paridad(char(tx));
        while true
            transmissions=transmissions+1;
            [rx, errorrate] = canalBS(txt, 0.9); % THE FLOW IN RECEPTION IS OBTAINED
            [rvalor, rxt] = paridad(char(rx(1:length(rx)-1)));
            rvalor;
            tvalor;
            if strcmp(tvalor, rvalor)
                break
            end
        end
        if strcmp(rxt,txt) % CHECK IF TX WAS CORRECT
                exito = exito + 1;
        end
    end
    errorrate; 
    exito;
    r = exito / N
    t = transmissions / mensajes 
    tasa = r*1000*B/((B+1)*t)
end
end