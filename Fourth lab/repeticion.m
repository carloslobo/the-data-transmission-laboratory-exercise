function [nuevoflujo] = repeticion(flujo, R)
% Repeats R times each bit of the flow and returns it in “nuevoflujo”
len = length(flujo);
nuevoflujo = '';
for i = 1:len
    for j = 1:R
        nuevoflujo = nuevoflujo + string(flujo(i));
    end
end
nuevoflujo=char(nuevoflujo);
end