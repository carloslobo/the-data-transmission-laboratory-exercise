function [r,tasa] = example35
N = 1000;
f = ascii('hola mundo'); 
exito = 0;
transmissions = 0;
tx=f(1:7);
mensajes = 0;
for i=1:N
    mensajes = mensajes + 1;
    [tcode, tblock] = hamming(char(tx));
    transmissions=transmissions+1;
    while true
        [rx, errorrate] = canalBS(tblock, 0.9); % THE FLOW IN RECEPTION IS OBTAINED
        rcodeb = [rx(1), rx(2), rx(4)];
        rx = [rx(3),rx(5:7)]
        [rcodea, rblock] = hamming(rx);
        failon=0;
        if ~strcmp(rcodea, rcodeb)
            for k = 1:3
                if ~strcmp(rcodea(k),rcodea(k))
                    failon = failon + pow2(k-1) 
                end
            end
            
            if strcmp(rblock(k),'1')
                rblock(k)='0';
            else
                rblock(k)='1';
            end
            if strcmp(rblock, tblock)
                break
            end
        else
            break
        end
        transmissions=transmissions+1;
    end
    if strcmp(rblock, tblock) % CHECK IF TX WAS CORRECT
            exito = exito + 1;
    end
end
errorrate; 
exito;
r = exito / N
t = transmissions / mensajes 
tasa =  r*1000*4/7
end