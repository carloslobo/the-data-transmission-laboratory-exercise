function freq = calculofrecuencias ( nombre_fichero )
% Calculation of the occurrence frequency of each character
% In the indicated name. Returns array of 256 positions
% Indexed by the corresponding character
F = fopen(nombre_fichero,'r');
f = nan(1,256); %f is an array with number of showing up for each character
freq = nan(1,256);
sum = 0;
while ~feof(F) 
    c = fread(F,1);
    if(isnan(f(c+1)))
        f(c+1) = 1;
    else
        f(c+1) = f(c+1) + 1;
    end
end
for i=1:256
    if(~isnan(f(i)))
        sum = sum + f(i);
    end
end

freq = f/sum;
H = entropia(freq)


% Include graphical representation of frequencies
figure;
plot(freq);

end
