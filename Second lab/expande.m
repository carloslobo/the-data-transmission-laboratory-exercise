function [nuevocodigo] = expande ( codigo, indA, indB )

% This function expands the Huffman tree and creates the associated code
% to each symbol of the alphabet, we use strings to store the strings
% and concatenation operations to expand them
% Internally, it copies the contents of indA to the indB
% and re-tag indA by adding a 0 and indB by adding a 1
codigo{indB} = codigo{indA};
codigo{indA} = [codigo{indA}, '0']; 
codigo{indB} = [codigo{indB}, '1'];

nuevocodigo = codigo;
end