function [codigo] = huffman_string(texto)
%Function calculate codigo from text and calls codifica

freq = nan(1,256);
for i=1:length(texto)
    if(isnan(freq(texto(i)+1)))
        freq(texto(i)+1) = 1;
    else
        freq(texto(i)+1) = freq(texto(i)+1) + 1;
    end
end
freq
codigo = huffman(freq)
codifica(texto,codigo);
end