function  [nuevotexto, longitud]  = codifica( texto, codigo ) 

% Transforms the text to the indicated encoding. Returns “nuevotexto” with 
% encoding, length with the number of binary digits of the same  
nuevotexto = ''; 

% Let's expand the “nuevotexto” string with the bytes of the next symbol 
% Note that the code numbers the symbols from 1, but in text the first index is 0 
for i=1:length(texto) 
    char = texto(i);
    nuevotexto = strcat(nuevotexto,codigo(char + 1));
end 

% We calculate “nuevotexto” length 
nuevotexto
longitud = strlength(nuevotexto(1))

end