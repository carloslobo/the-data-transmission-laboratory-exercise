function [codigo] = calculatehuffman( file )

p = calculofrecuencias(file);
codigo = huffman(p);
sum = 0;
n = 0;
for i=1:256
    if(~strcmp(codigo(i),""))
    sum = sum + strlength(codigo(i));
    n = n + 1;
    end
end
L = sum/n;
end