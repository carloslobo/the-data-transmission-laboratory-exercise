function  [codigo]  = huffman ( p ) 

% This function calculates the Huffman encoding for an alphabet 

% Where the frequency of occurrence of the symbols is given by p 

L = length(p); % Length of the alphabet 
codigo = repmat({''}, 1, L); % Vector to save the codes 
iter = 0; % Counter of number of iterations performed by �compacta� 
idx = zeros(1, 2); % Matrix to save indexes that change due to iter
indA = 1;
while (indA ~= -1)
    [indA,indB,acumulado,nuevop] = compacta(p);
    iter = iter + 1;
    idxA(iter) = indA;
    idxB(iter) = indB;
    p = nuevop;
end
iter = iter - 1;
for i = 0:iter-1
    codigo = expande(codigo,idxA(iter-i),idxB(iter-i));
end
end 