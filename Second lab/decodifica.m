function  [longitud, texto, nuevoflujo] = decodifica( flujo, codigo ) 

% Decodes the flow with the given code (prefix type) 
% Returns number of decoded symbols, text with symbols, and the excess part of the flow if it contains no symbols 
texto = ''; 
longitud = 0;
code = '';
n=1;
nuevoflujo ='';
temp = char(flujo);
% While condition 

% A sliding window must be created where in each phase 
% 1. Enter a new digit from the stream 
% 2. Check if the contents of the window match some code 
% 3. If yes, the window is reset, the symbol is added corresponding to the text, and the used digits are deleted % from the stream 

% Note that the code numbers the symbols from 1, but  in text the first index is 0 
% End 

while (~isempty(temp))
    code = strcat(code,temp(n));
    for i=1:256
        if(strcmp(code,codigo(i)))
          texto = [texto, char(i-1)];
           temp = temp((strlength(code)+1):end);
          code = '';
          n = 0; 
          break;
        end   
    end
    n = n+1;
end
longitud = length(texto);
texto
end 