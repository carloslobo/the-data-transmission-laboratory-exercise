function [indA, indB, acumulado, nuevop] = compacta ( p )
% This function looks for the alphabet indexes that have smaller
% probability and returns the cumulative of your probabilities
% Also returns a new vector with the same assignment of
% probabilities, but where indA is replaced by cumulative and indB
% by NaN
% If the vector can not be reduced it returns -1, -1, 0, []
[minpA,minIndexA] = min(p);
p(minIndexA) = NaN;
[minpB,minIndexB] = min(p);
 
if(~isnan(minpB))
    newp = minpA + minpB;
    indA = minIndexA;
    indB = minIndexB;
    nuevop = p;
    acumulado = newp;
    nuevop(minIndexA) = newp;
    nuevop(minIndexB) = NaN(1);
else
    indA = -1;
    indB = -1;
    acumulado = 0;
    nuevop = [];
end
end