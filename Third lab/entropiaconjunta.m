function [HXY, HX, HY, HXcondY, HYcondX] = entropiaconjunta(P)
%Calculo de la entropia conjunta de la variable XY, expresada por una 
% matriz P con la funcion de masa conjunta, siendo P(i,j)=prob(X=i,Y=j)   
[n,m]= size(P);
%probabilitys that I need
px=zeros(n,1);
py=zeros(1,m);

%entropia that i need
HX = 0;
HY = 0;
HXY = 0;
HXcondY = 0;
HYcondX = 0;
for i=1:n
    suma=0;
    for j=1:m
        suma=P(i,j)*log2(P(i,j));
        if isnan(suma)
            suma=0; 
        end
        HXY=HXY-suma;
       
    end
    
end
P;
px=sum(P,2);
py=sum(P);
HX=entropia(px);
HY=entropia(py);
HXcondY=HXY-HY;
HYcondX=HXY-HX;

end




%H(X|Y)=sum(P(X,Y)*log2(P(X,Y)/(P(X)*P(Y)))