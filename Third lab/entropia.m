function [H] = entropia( freq )
sum = 0;
  for p = 1:length(freq)
   if (~(0 ==(freq(p)))) 
     sum = sum + freq(p)*log2(freq(p));
     
   end
 end
H= - sum;
end