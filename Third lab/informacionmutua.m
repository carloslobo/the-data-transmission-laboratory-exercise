function I = informacionmutua(P)
% Calculo de la informacion mutua de las variables X e Y
% P proporciona su masa conjunta
[HXY, HX, HY, HXcondY, HYcondX] = entropiaconjunta(P);

I = HX-HXcondY;
end
