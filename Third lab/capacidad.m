function [C, pX] = capacidad(Q)
% Calculates the capacity of a channel defined
% by its Q matrix
% It also returns the mass of X
% The function combinacionesX(L)returns all possible
% combinations for the mass of X, where L is the
% number of symbols of the alphabet of X
maxi = 0;
comb = [];
pom1 = size(Q,1);
pom2 = size(Q,2);
X = combinacionesX(pom1);
pom3 = size(X,1);
for i = 1 : pom3
 for j = 1: pom2
 P(j,:) = Q(j,:)*X(i,j);
 end
 I = informacionmutua(P);
 if (I > maxi)
 maxi = I;
 comb = [X(i,1) X(i,2)];
 end
end
pX = comb;
C = maxi;
end
